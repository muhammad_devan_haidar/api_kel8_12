/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.4.28-MariaDB : Database - db_project_kel12
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_project_kel12` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

USE `db_project_kel12`;

/*Table structure for table `data_barang` */

DROP TABLE IF EXISTS `data_barang`;

CREATE TABLE `data_barang` (
  `kode_barang` char(12) NOT NULL,
  `nama_barang` varchar(100) DEFAULT NULL,
  `jumlah` char(12) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `data_barang` */

insert  into `data_barang`(`kode_barang`,`nama_barang`,`jumlah`) values 
('1A2A3A','Buku Pedoman YGY','500'),
('1B2B3B','Buku Devan 1','10'),
('4A4B4C','Gigi Doni gede','21');

/*Table structure for table `data_pengajuan` */

DROP TABLE IF EXISTS `data_pengajuan`;

CREATE TABLE `data_pengajuan` (
  `kode_pengajuan` char(12) NOT NULL,
  `tanggal` varchar(20) DEFAULT NULL,
  `npm_peminjam` varchar(12) DEFAULT NULL,
  `nama_peminjam` varchar(50) DEFAULT NULL,
  `prodi` varchar(50) DEFAULT NULL,
  `no_handphone` char(15) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `data_pengajuan` */

insert  into `data_pengajuan`(`kode_pengajuan`,`tanggal`,`npm_peminjam`,`nama_peminjam`,`prodi`,`no_handphone`) values 
('0001','2023-06-23','20311448','Devan haidar','Sistem Informasi','089602851592'),
('0002','2023-06-13','20311442','Yuda','sistem','081239812938');

/*Table structure for table `data_pengajuan_detail` */

DROP TABLE IF EXISTS `data_pengajuan_detail`;

CREATE TABLE `data_pengajuan_detail` (
  `kode_pengajuan` char(12) DEFAULT NULL,
  `kode_barang` char(12) DEFAULT NULL,
  `jumlah` char(12) DEFAULT NULL,
  KEY `kode_pengajuan` (`kode_pengajuan`),
  KEY `kode_barang` (`kode_barang`),
  CONSTRAINT `data_pengajuan_detail_ibfk_1` FOREIGN KEY (`kode_pengajuan`) REFERENCES `data_pengajuan` (`kode_pengajuan`),
  CONSTRAINT `data_pengajuan_detail_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `data_barang` (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `data_pengajuan_detail` */

insert  into `data_pengajuan_detail`(`kode_pengajuan`,`kode_barang`,`jumlah`) values 
('0001','1A2A3A','1');

/*Table structure for table `data_pengembalian` */

DROP TABLE IF EXISTS `data_pengembalian`;

CREATE TABLE `data_pengembalian` (
  `kode_pengembalian` char(12) NOT NULL,
  `kode_pengajuan` char(12) DEFAULT NULL,
  `tanggal_kembali` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian`),
  KEY `kode_pengajuan` (`kode_pengajuan`),
  CONSTRAINT `data_pengembalian_ibfk_1` FOREIGN KEY (`kode_pengajuan`) REFERENCES `data_pengajuan` (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `data_pengembalian` */

insert  into `data_pengembalian`(`kode_pengembalian`,`kode_pengajuan`,`tanggal_kembali`) values 
('0001','0001','2023-06-13'),
('0002','0002','2023-06-14'),
('0005','0002','2024-04-20');

/*Table structure for table `data_pengembalian_detail` */

DROP TABLE IF EXISTS `data_pengembalian_detail`;

CREATE TABLE `data_pengembalian_detail` (
  `kode_pengembalian` char(12) DEFAULT NULL,
  `kode_barang` char(12) DEFAULT NULL,
  `jumlah` char(12) DEFAULT NULL,
  KEY `kode_pengembalian` (`kode_pengembalian`),
  KEY `kode_barang` (`kode_barang`),
  CONSTRAINT `data_pengembalian_detail_ibfk_1` FOREIGN KEY (`kode_pengembalian`) REFERENCES `data_pengembalian` (`kode_pengembalian`),
  CONSTRAINT `data_pengembalian_detail_ibfk_2` FOREIGN KEY (`kode_barang`) REFERENCES `data_barang` (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `data_pengembalian_detail` */

insert  into `data_pengembalian_detail`(`kode_pengembalian`,`kode_barang`,`jumlah`) values 
('0001','1A2A3A','1');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `user` */

insert  into `user`(`id_user`,`name`,`username`,`pass`,`role`) values 
(8,'devan11','devanhaidar','827ccb0eea8a706c4c34a16891f84e7b','Admin'),
(9,'jimmi julianto','jimmi','674f3c2c1a8a6f90461e8a66fb5550ba','Admin'),
(10,'ardiyansyah','ardi','8c3f0a6e929c3d22358b8ec5a492fd36','Admin'),
(11,'feri cahya','feri','4e25c2fb937c97773de6293048ca99ab','Admin'),
(12,'aryuda','yuda','c46c57ba69255b5626c657f9a08b23d5','Admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
